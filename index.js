// 0A. Lấy phần tử bằng ID
function getEle(n) {
    return document.getElementById(n);
}

// 0B. Nhập phần tử của mảng
function getNumber() {
    var n = Number(getEle("inputNum").value);
    numArray.push(n), (getEle("txtArray").innerHTML = numArray);
}

// 01. Tính tổng số dương trong mảng
function sumPositive() {
    for (var n = 0, r = 0; r < numArray.length; r++) numArray[r] > 0 && (n += numArray[r]);
    getEle("txtSum").innerHTML = "Tổng số dương: " + n;
}

// 02. Tính số lượng số dương trong mảng
function countPositive() {
    for (var n = 0, r = 0; r < numArray.length; r++) numArray[r] > 0 && n++;
    getEle("txtCount").innerHTML = "Số dương: " + n;
}

// 03. Tìm số nhỏ nhất trong mảng
function findMin() {
    for (var n = numArray[0], r = 1; r < numArray.length; r++) numArray[r] < n && (n = numArray[r]);
    getEle("txtMin").innerHTML = "Số nhỏ nhất: " + n;
}

// 04. Tìm số dương nhỏ nhất trong mảng
function findMinPos() {
    for (var n = [], r = 0; r < numArray.length; r++) numArray[r] > 0 && n.push(numArray[r]);
    if (n.length > 0) {
        for (var e = n[0], r = 1; r < n.length; r++) n[r] < e && (e = n[r]);
        getEle("txtMinPos").innerHTML = "Số dương nhỏ nhất: " + e;
    } else getEle("txtMinPos").innerHTML = "Không có số dương trong mảng";
}

// 05. Tìm số chẵn cuối cùng trong mảng (nếu mảng không có giá trị chẵn thì trả về -1)
function findEven() {
    for (var n = 0, r = 0; r < numArray.length; r++) numArray[r] % 2 == 0 && (n = numArray[r]);
    getEle("txtEven").innerHTML = "Số chẵn cuối cùng: " + n;
}

// 06. Đổi chỗ 2 giá trị trong mảng theo vị trí
function swap(n, r) {
    var e = numArray[n];
    (numArray[n] = numArray[r]), (numArray[r] = e);
}
function changePosition() {
    swap(getEle("inputIndex1").value, getEle("inputIndex2").value), (getEle("txtChangePos").innerHTML = "Mảng sau khi đổi: " + numArray);
}

// 07. Sắp xếp mảng theo thứ tự tăng dần
function sortIncrease() {
    for (var n = 0; n < numArray.length; n++) for (var r = 0; r < numArray.length - 1; r++) numArray[r] > numArray[r + 1] && swap(r, r + 1);
    getEle("txtIncrease").innerHTML = "Mảng sau khi sắp xếp: " + numArray;
}

// 08. Tìm số nguyên tố đầu tiên trong mảng (nếu mảng không có số nguyên tố thì trả về -1)
function checkPrime(n) {
    if (n < 2) return !1;
    for (var r = 2; r <= Math.sqrt(n); r++) if (n % r == 0) return !1;
    return !0;
}
function findPrime() {
    for (var n = -1, r = 0; r < numArray.length; r++) {
        if (checkPrime(numArray[r])) {
            n = numArray[r];
            break;
        }
    }
    getEle("txtPrime").innerHTML = n;
}

// 09. Tìm số lượng số nguyên có trong mảng (sau khi nhập thêm một mảng số thực)
function getFloat() {
    var n = Number(getEle("inputFloat").value);
    arrayFloat.push(n), (getEle("txtArrayFloat").innerHTML = arrayFloat);
}
function findInt() {
    for (var n = 0, r = 0; r < arrayFloat.length; r++) Number.isInteger(arrayFloat[r]) && n++;
    getEle("txtInt").innerHTML = "Số nguyên: " + n;
}

// 10. So sánh số lượng số dương và số lượng số âm
function compareNum() {
    for (var n = 0, r = 0, e = 0; e < numArray.length; e++) numArray[e] > 0 ? n++ : numArray[e] < 0 && r++;
    getEle("txtCompare").innerHTML = n > r ? "Số dương > Số âm" : n < r ? "Số âm > Số dương" : "Số âm = Số dương";
}

document.addEventListener(
    "contextmenu",
    function (n) {
        n.preventDefault();
    },
    !1
),
    (document.onkeydown = function (n) {
        return 123 != (n = n || window.event).keyCode && (!n.ctrlKey || !n.shiftKey || 73 != n.keyCode) && void 0;
    });
var numArray = [],
    arrayFloat = [];
